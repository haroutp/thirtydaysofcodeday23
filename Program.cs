﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
class Node{
    public Node left,right;
    public int data;
    public Node(int data){
        this.data=data;
        left=right=null;
    }
}
class Solution{

	static void levelOrder(Node root){
  		//Write your code here
        Queue<Node> q = new Queue<Node>();

        if(root == null){
            return;
        }
        Node cur = root;
        Console.Write(cur.data + " ");
        while(cur.left != null || cur.right != null || q.Count > 0){
            if(cur.left != null){
                q.Enqueue(cur.left);
            }
            if(cur.right != null){
                q.Enqueue(cur.right);
            }
            cur = q.Dequeue();
            Console.Write(cur.data + " ");
        }
        
            

    }

	static Node insert(Node root, int data){
        if(root==null){
            return new Node(data);
        }
        else{
            Node cur;
            if(data<=root.data){
                cur=insert(root.left,data);
                root.left=cur;
            }
            else{
                cur=insert(root.right,data);
                root.right=cur;
            }
            return root;
        }
    }
    static void Main(String[] args){
        Node root=null;
        int T=Int32.Parse(Console.ReadLine());
        while(T-->0){
            int data=Int32.Parse(Console.ReadLine());
            root=insert(root,data);            
        }
        levelOrder(root);
        
    }
}